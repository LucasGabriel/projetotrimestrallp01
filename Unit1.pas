unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Lista: TListBox;
    Tipo: TLabel;
    Modelo: TLabel;
    Cor: TLabel;
    Enviar: TButton;
    Deletar: TButton;
    Atualizar: TButton;
    Salvar: TButton;
    Importar: TButton;
    ECor: TEdit;
    ETipo: TEdit;
    EModelo: TEdit;
    procedure EnviarClick(Sender: TObject);
    procedure DeletarClick(Sender: TObject);
    procedure AtualizarClick(Sender: TObject);
    procedure SalvarClick(Sender: TObject);
    procedure ImportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

type
TInstrumento = class(TObject)
    Tipo,Cor,Modelo : string;
    end;
var Instrumento : TInstrumento;


{$R *.dfm}

procedure TForm1.AtualizarClick(Sender: TObject);
begin
Lista.Items[Lista.ItemIndex]:= ETipo.Text;
Instrumento.Cor := ECor.Text;
Instrumento.Modelo :=EModelo.text;

  ETipo.Text:='';
  ECor.Text:='';
  EModelo.Text:='';

end;

procedure TForm1.DeletarClick(Sender: TObject);
begin
Lista.DeleteSelected;
end;

procedure TForm1.EnviarClick(Sender: TObject);
begin
  Instrumento := TInstrumento.Create;
  Instrumento.Tipo := ETipo.text;
  Instrumento.Cor := ECor.Text;
  Instrumento.Modelo :=EModelo.text;

  if (Instrumento.Tipo <> '') and (Instrumento.Cor <> '') and (Instrumento.Modelo <> '')  then
  begin
  Lista.Items.Add(Instrumento.Tipo);
  ETipo.Text:='';
  ECor.Text:='';
  EModelo.Text:='';
  end
  else begin
  ShowMessage('Algum dos campos esta vazio');
  end;


end;

procedure TForm1.ImportarClick(Sender: TObject);
begin
Lista.Items.LoadFromFile ('Instrumento.txt');
end;

procedure TForm1.SalvarClick(Sender: TObject);
 var Atributos: TextFile;
    InstrumentoAtri: TInstrumento;
    i: integer;
begin
AssignFile(Atributos,'Instrumento.txt');
Rewrite(Atributos);
for i:=0 to Pred(Lista.Items.Count) do
begin
InstrumentoAtri:= TInstrumento(Lista.Items.Objects[i]);
writeln(Atributos,'Tipo: ', Instrumento.Tipo);
writeln(Atributos,'Cor: ',Instrumento.Cor);
writeln(Atributos,'Modelo: ',Instrumento.Modelo);
      end;
        CloseFile(Atributos);
end;

end.
