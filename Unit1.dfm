object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 410
  ClientWidth = 265
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Tipo: TLabel
    Left = 177
    Top = 16
    Width = 20
    Height = 13
    Caption = 'Tipo'
  end
  object Cor: TLabel
    Left = 177
    Top = 43
    Width = 17
    Height = 13
    Caption = 'Cor'
  end
  object Modelo: TLabel
    Left = 177
    Top = 70
    Width = 34
    Height = 13
    Caption = 'Modelo'
  end
  object ETipo: TEdit
    Left = 50
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object ECor: TEdit
    Left = 50
    Top = 35
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object EModelo: TEdit
    Left = 50
    Top = 62
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object Enviar: TButton
    Left = 50
    Top = 255
    Width = 75
    Height = 25
    Caption = 'Enviar'
    TabOrder = 3
    OnClick = EnviarClick
  end
  object Atualizar: TButton
    Left = 50
    Top = 303
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    TabOrder = 4
    OnClick = AtualizarClick
  end
  object Importar: TButton
    Left = 96
    Top = 342
    Width = 75
    Height = 25
    Caption = 'Importar'
    TabOrder = 5
    OnClick = ImportarClick
  end
  object Deletar: TButton
    Left = 136
    Top = 255
    Width = 75
    Height = 25
    Caption = 'Deletar'
    TabOrder = 6
    OnClick = DeletarClick
  end
  object Salvar: TButton
    Left = 136
    Top = 303
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 7
    OnClick = SalvarClick
  end
  object Lista: TListBox
    Left = 50
    Top = 112
    Width = 161
    Height = 122
    ItemHeight = 13
    TabOrder = 8
  end
end
